async function login(page, course) {
    console.log('loging in');
    course = course.split('/').join('%2F');
    await page.goto(`https://app.pluralsight.com/id?redirectTo=${course}`, { timeout: 0 });
    await page.type('#Username', process.env.EMAIL)
    await page.type('#Password', process.env.PASSWORD)
    await page.click('[id="login"]')
}

async function openCourse(page) {
    console.log('opening course');
    await sleep(10000);
    await page.waitForSelector('[class = "button button--medium course-hero__button"]')
    await page.click('[class = "button button--medium course-hero__button"]')
}

async function watchCourse(wait = 3000) {
    console.log('watching course');
    await sleep(wait);
}


function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

module.exports = { login, watchCourse, openCourse }