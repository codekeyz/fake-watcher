const puppeteer = require('puppeteer-core');
const pluralsight = require('./pluralsight');


async function bootstrap(course = process.env.WATCHURL) {
    // console.log(`Email: ${process.env.EMAIL}\nPassword: ${process.env.PASSWORD}\nWatchURL: ${process.env.WATCHURL}`);

    const browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
        // headless: false,
        executablePath: '/usr/bin/google-chrome'
    });

    let pages = await browser.pages();

    await pluralsight.login(pages[0], course);

    await pluralsight.openCourse(pages[0]);

    await pluralsight.watchCourse(3600000);

    await browser.close();

    return true;
}

try {
    bootstrap();
} catch (e) {
    console.log(e);
}
